# Quicknotes

Quicknotes is an electron/es6 app for keeping notes without fighting with the app.

>Basic idea behind this app was to make something while learning electron/es6. Hope can help others to catch up with this nice technologies for cross platform desktop apps. Open to ideas and tips how to make code and app better but without losing the easy understanding by a newbie

![](icons/qnscreenshot.png)

### Installation

- npm install -g electron-prebuilt
- git clone git@gitlab.com:ditsikts/quicknotes.git
- cd quicknotes
- npm install

And run with:
  - npm start

### License

The project is licensed under the MIT license.
